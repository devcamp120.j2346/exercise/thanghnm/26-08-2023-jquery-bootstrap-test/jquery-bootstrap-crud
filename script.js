$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gBASE_URL = "https://63b401c1ea89e3e3db539072.mockapi.io/api/v1/";
  const gPRODUCT_COLUMNS = ["id", "productCode", "productName", "categoryId", "countryId", "imagUrl", "price", "discountPrice", "action"]
  const gCOLUMN_STT = 0
  const gCOLUMN_PRODUCT_CODE = 1
  const gCOLUMN_PRODUCT_NAME = 2
  const gCOLUMN_CATEGORY_ID = 3
  const gCOLUMN_COUNTRY_ID = 4
  const gCOLUMN_IMAGE_URL = 5
  const gCOLUMN_PRICE = 6
  const gCOLUMN_DISCOUNT_PRICE = 7
  const gCOLUMN_ACTION = 8
  var gStt = 0
  var gArray = []
  var gProduct
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading()
  var gTable = $("#table-product").DataTable({
    // Phân trang
    paging: true,
    // Tìm kiếm
    searching: false,
    // Sắp xếp
    ordering: false,
    // Số bản ghi phân trang
    lengthChange: false,
    columns: [
      { data: gPRODUCT_COLUMNS[gCOLUMN_STT] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_PRODUCT_CODE] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_PRODUCT_NAME] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_CATEGORY_ID] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_COUNTRY_ID] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_IMAGE_URL] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_PRICE] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_DISCOUNT_PRICE] },
      { data: gPRODUCT_COLUMNS[gCOLUMN_ACTION] }


    ],
    columnDefs: [
      {
        targets: gCOLUMN_STT,
        render: renderStt
      },
      {
        targets: gCOLUMN_ACTION,
        defaultContent: `<button class="btn btn-edit">
        <i class="fa-solid fa-pen-to-square"></i>
        </button>
        <button class="btn btn-delete">
        <i class="fa-solid fa-trash-can"></i>
        </button>`
      },
      {
        targets: gCOLUMN_IMAGE_URL,
        render: renderImg
      },
      {
        targets: gCOLUMN_CATEGORY_ID,
        render: renderCategory
      },
      {
        targets: gCOLUMN_COUNTRY_ID,
        render: renderCountry
      }
    ]
  })
  // Gán sự kiện cho nút edit
  $(document).on("click", ".btn-edit", function () {
    onBtnEditClick(this)

  })
  // Gán sự kiện cho nút filter
  $(document).on("click", "#btn-filter", function () {
    onBtnFilterClick()
  })
  // Gán sự kiện cho nút update product trong modal edit
  $(document).on("click", "#btn-update-product", function () {
    onBtnUpdateProductClick()
  })
  //Gán sự kiện cho nút thêm sản phẩm
  $(document).on("click", ".add-new-product", function () {
    $("#add-modal").modal("show")
  })
  // Gán sự kiện cho nút confirm thêm user
  $(document).on("click", "#btn-add-product", function () {
    onBtnAddNewProductClick()
  })
  // Gán sự kiện cho nút Delete
  $(document).on("click", ".btn-delete", function () {
    onBtnDeleteClick(this)
  })
  // Gán sự kiện cho nút confirm delete
  $(document).on("click", "#btn-confirm-delete-product", function () {
    onBtnConfirmDeleteClick()
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Gọi Api
    processApiToLoadListProducts()
    processApiToLoadListCateGory()
    processApiToLoadLisCountry()
  }
  // Hàm xử lý khi click nút edit
  function onBtnEditClick(paramBtn) {
    $("#edit-modal").modal("show")
    var vRowSelected = $(paramBtn).parents("tr")
    var vDataRow = gTable.row(vRowSelected).data()
    $("#input-edit-product-code").val(vDataRow.productCode)
    $("#input-edit-product-name").val(vDataRow.productName)
    $("#select-edit-category").val(vDataRow.categoryId)
    $("#select-edit-country").val(vDataRow.countryId)
    $("#img-edit-product").prop("src", vDataRow.imagUrl)
    $("#input-edit-price").val(vDataRow.price)
    $("#input-edit-discount").val(vDataRow.discountPrice)
    $("#btn-update-product").attr("productId", vDataRow.id)
    console.log(vDataRow)
  }
  // Hàm xử lý nút filter
  function onBtnFilterClick() {
    var vObjectFilter = {
      categoryId: 0,
      countryId: 0,
    }
    // B1 Thu thập dữ liệu
    getDataToFilter(vObjectFilter)
    // B2 Kiểm tra dữ liệu[Không có]
    // B3 Tìm kiếm và xử lý hiển thị
    var vArrUserFiltered = filterTable(vObjectFilter)
    var vTable = $("#table-product")
    vTable.find("tr:gt(0)").html("");
    if (vArrUserFiltered.length > 0) {
      alert("Tìm thấy " + vArrUserFiltered.length + " Kết quả phù hợp");
      displayDataToTable(vArrUserFiltered)
    }
    else {
      alert("không tìm thấy kết quả phù hợp")
      return false
    }
  }
  function onBtnUpdateProductClick() {
    var vObject = {
      id: 0,
      productCode: "",
      productName: "",
      categoryId: 0,
      countryId: 0,
      imagUrl: "",
      price: "",
      discountPrice: ""
    }
    // B1 Thu thập dữ liệu
    getDataToUpdateProduct(vObject)
    // B2 kiểm tra dữ liệu
    var vValidate = isProductDataValidate(vObject)
    if (vValidate == true) {
      // B3 Gọi Api và xử lý hiển thị
      processApiToUpdateProduct(vObject)
    }

  }
  function onBtnAddNewProductClick() {
    var vObject = {
      productCode: "",
      productName: "",
      categoryId: 0,
      countryId: 0,
      imagUrl: "",
      price: "",
      discountPrice: ""
    }
    // B1 Thu thập dữ liệu
    getDataToAddProduct(vObject)
    // B2 Kiểm tra dữ liệu
    var vValidate = isProductDataValidateToAdd(vObject)
    if (vValidate == true) {
      //B3 Gọi Api để thêm vào danh sách
      processApiToAddProducts(vObject)

    }
  }
  function onBtnConfirmDeleteClick(paramButton) {
    // B1 Lấy Id của product cần xóa
    var vProductId = $("#btn-confirm-delete-product").attr("productId")
    processDeleteProduct(vProductId)
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm gọi Api để lấy dữ liệu từ server cho việc hiển thị ra site
  function processApiToLoadListProducts() {
    $.ajax({
      url: gBASE_URL + "products/",
      type: "GET",
      success: function (responseObject) {
        displayDataToTable(responseObject)
        gProduct = responseObject
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm gọi Api để lấy nhóm sản phẩm
  function processApiToLoadListCateGory() {
    $.ajax({
      url: gBASE_URL + "categories/",
      type: "GET",
      async: false,
      success: function (responseObject) {
        gArray.push(responseObject)
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm gọi Api để lấy country
  function processApiToLoadLisCountry() {
    $.ajax({
      url: gBASE_URL + "countries/",
      type: "GET",
      async: false,
      success: function (responseObject) {
        gArray.push(responseObject)
        // B4 xử lý hiển thị
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm render Cột số thứ tự
  function renderStt() {
    gStt++
    return gStt
  }
  // Hàm load vào bảng
  function displayDataToTable(paramObject) {
    gTable.clear();
    gTable.rows.add(paramObject);
    gTable.draw();
  }
  //Hàm render Img
  function renderImg(paramUrl) {
    return `<img class="img-product" src="${paramUrl}"/>`
  }
  // Hàm render Category
  function renderCategory(paramIdCategory) {
    var vCategory = null;
    var vFound = false;
    var vVitrihientai = 0;
    while (vFound == false && vVitrihientai < gArray[0].length) {
      if (gArray[0][vVitrihientai].id == paramIdCategory) {
        vFound = true
        vCategory = gArray[0][vVitrihientai].categoryName
      }
      vVitrihientai++;
    }
    return vCategory

  }
  // Hàm render Category
  function renderCountry(paramIdCountry) {
    var vCountry = null;
    var vFound = false;
    var vVitrihientai = 0;
    while (vFound == false && vVitrihientai < gArray[1].length) {
      if (gArray[1][vVitrihientai].id == paramIdCountry) {
        vFound = true
        vCountry = gArray[1][vVitrihientai].countryName
      }
      vVitrihientai++;
    }
    return vCountry

  }
  // hàm thu thập dữ liệu để filter
  function getDataToFilter(paramObject) {
    paramObject.categoryId = $("#filter-category").val()
    paramObject.countryId = $("#filter-country").val()
  }
  // Hàm filter
  function filterTable(paramObject) {
    const bArrayResultObject = []
    // Trường hợp nhập cả firstname và lastname
    if (paramObject.categoryId != 0 && paramObject.countryId != 0) {
      for (var bI = 0; bI < gProduct.length; bI++) {
        const bItemObject = gProduct[bI]
        //So sánh
        if (paramObject.categoryId == bItemObject.categoryId
          && paramObject.countryId == bItemObject.countryId) {
          // Thêm đối tượng thỏa mãn vào array
          bArrayResultObject.push(bItemObject)
        }
      }
    }
    //trường hợp nhập 1 trong 2 ô input để filter
    else if (paramObject.categoryId != 0 || paramObject.countryId != 0) {
      for (var bI = 0; bI < gProduct.length; bI++) {
        const bItemUserObject = gProduct[bI]
        //So sánh
        if (paramObject.categoryId == bItemUserObject.categoryId ||
          paramObject.countryId == bItemUserObject.countryId) {
          // Thêm đối tượng thỏa mãn vào array
          bArrayResultObject.push(bItemUserObject)
          console.log(bItemUserObject)
        }
      }
    }
    return bArrayResultObject;
  }
  // Hàm thu thập dữ liệu để update product
  function getDataToUpdateProduct(paramObject) {
    paramObject.productCode = $.trim($("#input-edit-product-code").val())
    paramObject.productName = $.trim($("#input-edit-product-name").val())
    paramObject.categoryId = $("#select-edit-category").val()
    paramObject.countryId = $("#select-edit-country").val()
    paramObject.imagUrl = "https://azse77seaprodsa.blob.core.windows.net/b2b-dr-pickaboocdn/media/catalog/product/cache/90e3b9f4120fc209bf60003e3b0e1323/i/n/infinix-hot-8128-30i-glacier-blue-8.jpg"
    paramObject.price = $("#input-edit-price").val()
    paramObject.discountPrice = $("#input-edit-discount").val()
    paramObject.id = $("#btn-update-product").attr("productId")
  }
  // Hàm kiểm tra dữ liệu để update product
  function isProductDataValidate(paramObject) {
    if (paramObject.productCode == "") {
      alert("Hãy nhập product code")
      return false
    }
    if (paramObject.productName == "") {
      alert("Hãy nhập product code")
      return false
    }
    if (paramObject.price == "") {
      alert("Hãy nhập price")
      return false

    }
    if (paramObject.discountPrice == "") {
      alert("Hãy nhập discount price")
      return false

    }
    return true
  }
  // Hàm gọi Api để update product
  function processApiToUpdateProduct(paramObject) {
    $.ajax({
      url: gBASE_URL + "products/" + paramObject.id,
      type: "PUT",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      success: function (responseObject) {
        alert("Update Product thành công")
        $("#edit-modal").modal("hide")
        onPageLoading()
        // B4 xử lý hiển thị
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm thu thập dữ liệu để thêm mới sản phẩm
  function getDataToAddProduct(paramObject) {

    paramObject.productCode = $.trim($("#input-add-product-code").val())
    paramObject.productName = $.trim($("#input-add-product-name").val())
    paramObject.categoryId = $("#select-add-category").val()
    paramObject.countryId = $("#select-add-country").val()
    paramObject.imagUrl = $.trim($("#img-add-url").val())
    paramObject.price = $.trim($("#input-add-price").val())
    paramObject.discountPrice = $.trim($("#input-add-discount").val())
  }
  // Hàm kiểm tra dữ liệu
  function isProductDataValidateToAdd(paramObject) {
    if (paramObject.productCode == "") {
      alert("Hãy nhập product code")
      return false
    }
    if (paramObject.productName == "") {
      alert("Hãy nhập product code")
      return false
    }
    if (paramObject.price == "") {
      alert("Hãy nhập price")
      return false

    }
    if (paramObject.discountPrice == "") {
      alert("Hãy nhập discount price")
      return false

    }
    if (paramObject.imagUrl == "") {
      alert("Hãy nhập đường dẫn hình ảnh")
      return false

    }
    return true
  }
  // Hàm gọi Api để thêm sản phẩm
  function processApiToAddProducts(paramObject) {
    $.ajax({
      url: gBASE_URL + "products/",
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      success: function (response) {
        // B4: xử lý front-end
        alert(`
        Thêm sản phẩm thành công
        Mã sản phẩm là ${response.productCode}`)
        $("#add-modal").modal("hide")
        onPageLoading()

      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
  // Hàm gọi Api xóa Sản phẩm
  function processDeleteProduct(paramProductId) {
    $.ajax({
      url: gBASE_URL + "products/" + paramProductId,
      type: "DELETE",
      success: function (response) {
        alert("xóa voucher thành công");
        $("#delete-confirm-modal").modal("hide")
        onPageLoading()
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });

  }
  // Hàm xử lý khi click nút delete
  function onBtnDeleteClick(paramBtn) {
    $("#delete-confirm-modal").modal("show")
    var vRowSelected = $(paramBtn).parents("tr")
    var vDataRow = gTable.row(vRowSelected).data()
    $("#btn-confirm-delete-product").attr("productId", vDataRow.id)
  }
})